from tkinter import *
import time
import random

WIDTH = 1920
HEIGHT = 1080

tk = Tk()
canvas = Canvas(tk, width=WIDTH, height=HEIGHT, bg="black")
tk.title("Drawing")
canvas.pack()

colors = ['red', 'green', 'blue', 'orange', 'yellow', 'cyan', 'magenta',
          'dodgerblue', 'turquoise', 'grey', 'gold', 'pink']


class Ball:
    def __init__(self):
        self.size = random.randrange(3, 40)
        x_position = random.randrange(0, WIDTH - self.size - 1)
        y_position = random.randrange(0, HEIGHT - self.size - 1)
        self.shape = canvas.create_oval(x_position, y_position, x_position + self.size, y_position + self.size,
                                        fill=random.choice(colors))
        self.speedX = random.randrange(1, 15)
        self.speedY = random.randrange(1, 15)

    def update(self):
        canvas.move(self.shape, self.speedX, self.speedY)
        pos = canvas.coords(self.shape)
        if pos[2] >= WIDTH or pos[0] <= 0:
            self.speedX *= -1
        if pos[3] >= HEIGHT or pos[1] <= 0:
            self.speedY *= -1


ball_list = []

for i in range(80):
    ball_list.append(Ball())
while True:
    for ball in ball_list:
        ball.update()
    tk.update()
    time.sleep(0.01)
